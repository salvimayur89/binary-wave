//
//  HomeDetailViewController.swift
//  BinaryWave
//
//  Created by Neosoft on 08/02/22.
//

import UIKit

class HomeDetailViewController: UIViewController {
  let inset: CGFloat = 10
  let minimumLineSpacing: CGFloat = 10
  let minimumInteritemSpacing: CGFloat = 10
  let cellsPerRow = 3
  var selectedIndex = 0
  let kMoreImageCell = "HomeDetailCollectionViewCell"
  
  var vm = HomeViewModel()
  
  @IBOutlet var moreCollectionView: UICollectionView!
  override func viewDidLoad() {
    super.viewDidLoad()
    initialSetup()
    
  }
  func initialSetup(){
    self.navigationItem.title = vm.getRailTitle(index: selectedIndex)
    
    let image : UIImage? = UIImage.init(named: "backBtn")!.withRenderingMode(.alwaysOriginal)
    navigationItem.leftBarButtonItem = UIBarButtonItem(
      image: image,
      style: .plain,
      target: self,
      action: #selector(backButtonPressed)
    )
    setupCollectionView()
  }
  
  func setupCollectionView() {
    moreCollectionView?.contentInsetAdjustmentBehavior = .always
    moreCollectionView.delegate = self
    moreCollectionView.dataSource = self
    moreCollectionView.register(UINib(nibName: kMoreImageCell, bundle: nil), forCellWithReuseIdentifier: kMoreImageCell)
  }
  
  static func loadFromNib()-> HomeDetailViewController {
    return HomeDetailViewController(nibName: "HomeDetailViewController", bundle: nil)
  }
  
}
extension HomeDetailViewController: UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
    return vm.getRailDataCount(section: selectedIndex)
  }
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kMoreImageCell, for: indexPath) as! HomeDetailCollectionViewCell
    if let url = vm.getRailImageURl(row: indexPath.row, section: selectedIndex), !url.isEmpty {
      cell.moreImageView.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "Square")) { image, error, cacheType, url in
      }
    }
    return cell
  }
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
    return UIEdgeInsets(top: inset, left: inset, bottom: inset, right: inset)
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return minimumLineSpacing
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    return minimumInteritemSpacing
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    let marginsAndInsets = inset * 2 + collectionView.safeAreaInsets.left + collectionView.safeAreaInsets.right + minimumInteritemSpacing * CGFloat(cellsPerRow - 1)
    let itemWidth = ((collectionView.bounds.size.width - marginsAndInsets) / CGFloat(cellsPerRow)).rounded(.down)
    return CGSize(width: itemWidth, height: itemWidth)
  }
  
  override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
    moreCollectionView?.collectionViewLayout.invalidateLayout()
    super.viewWillTransition(to: size, with: coordinator)
  }
}

extension HomeDetailViewController {
  // MARK: - Navigation
  
  @objc func backButtonPressed(sender:UIButton) {
    self.navigationController?.popViewController(animated: true)
  }
}
