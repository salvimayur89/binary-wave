//
//  HomeViewController.swift
//  BinaryWave
//
//  Created by Neosoft on 07/02/22.
//

import UIKit

class HomeViewController: BaseViewController {
  
  @IBOutlet var homeTableView: UITableView!
  
  //TableView Cell Identifiers
  
  var kHomeCarouselCell = "HomeCarouselTableViewCell"
  var kHomeRailCell = "HomeRailTableViewCell"
  
  //CollectionView Cell Identifiers
  var kCircularRailCell = "CircularRailCollectionViewCell"
  var kSquareRailCell = "SquareRailCollectionViewCell"
  
  private lazy var homeVM: HomeViewModel = {
    return HomeViewModel()
  }()
  
  override func viewDidLoad() {
    self.navigationItem.title = "TenTime"
    super.viewDidLoad()
    fetchData()
  }
  func fetchData(){
    
    homeVM.getHomePageData(genre: "1") { (message) in
      if message.isEmpty {
        self.setupTableView()
      } else {
        
      }
    }
  }
  
  func setupTableView() {
    
    homeTableView.delegate = self
    homeTableView.dataSource = self
    
    homeTableView.register(UINib(nibName: kHomeCarouselCell, bundle: nil), forCellReuseIdentifier: kHomeCarouselCell)
    homeTableView.register(UINib(nibName: kHomeRailCell, bundle: nil), forCellReuseIdentifier: kHomeRailCell)
    
    
    homeTableView.estimatedRowHeight = 40.0
    homeTableView.rowHeight = UITableView.automaticDimension
    homeTableView.contentInsetAdjustmentBehavior = .never
  }
}

extension HomeViewController: UITableViewDelegate,UITableViewDataSource{
  func numberOfSections(in tableView: UITableView) -> Int {
    return 2
  }
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if section == 0 {
      return 1
    }
    return homeVM.getRailsCount()
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let cell = UITableViewCell()
    
    if indexPath.section == 0 {
      guard let carouselCell = tableView.dequeueReusableCell(withIdentifier: kHomeCarouselCell, for: indexPath) as? HomeCarouselTableViewCell else { return cell }
      carouselCell.setCarouselData(carouselLst: homeVM.getCarouselData())
      return carouselCell
    } else {
      guard let railCell = tableView.dequeueReusableCell(withIdentifier: kHomeRailCell, for: indexPath) as? HomeRailTableViewCell else { return cell }
      railCell.btnMoreOptions.tag = indexPath.row
      if homeVM.getRailDataCount(section: indexPath.row) > 10 {
        railCell.btnMoreOptions.isHidden = false
        railCell.btnMoreOptions.addTarget(self, action: #selector(btnMorePressed), for: .touchUpInside)
      } else {
        railCell.btnMoreOptions.isHidden = true
      }
      
      railCell.collectionViewHomeRails.tag = indexPath.row
      railCell.labelTitle.text = homeVM.getRailTitle(index: indexPath.row)
      
      railCell.collectionViewHomeRails.delegate = self
      railCell.collectionViewHomeRails.dataSource = self
      
      return railCell
    }
  }
  
  func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    if let tvCell = cell as? HomeRailTableViewCell {
      tvCell.collectionViewHomeRails.reloadData()
      
    }
  }
}

extension HomeViewController: UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    if homeVM.getRailDataCount(section: collectionView.tag) > 10 {
      return 10
    }
    return homeVM.getRailDataCount(section: collectionView.tag)
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
    switch homeVM.getRailCategory(index: collectionView.tag) {
    case "circle":
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kCircularRailCell, for: indexPath) as! CircularRailCollectionViewCell
      cell.trackData = homeVM.getRailTrack(row: indexPath.row, section: collectionView.tag)
      return cell
    case "square":
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kSquareRailCell, for: indexPath) as! SquareRailCollectionViewCell
      
      if let url = homeVM.getRailImageURl(row: indexPath.row, section: collectionView.tag), !url.isEmpty {
        cell.imgCoveer.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "Square")) { image, error, cacheType, url in
        }
      }
      
      return cell
    default:
      return UICollectionViewCell()
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    
    switch homeVM.getRailCategory(index: collectionView.tag) {
    case "circle":
      return CGSize(width: 150, height: 190)
    case "square":
      return CGSize(width: 150, height: 150)
    default:
      return CGSize(width: 0, height: 0)
    }
    
  }
  
}

extension HomeViewController {
  
  // MARK: - Navigation
  
  @objc func btnMorePressed(sender:UIButton) {
    let vc = HomeDetailViewController.loadFromNib()
    vc.vm = homeVM
    vc.selectedIndex = sender.tag
    self.navigationController?.pushViewController(vc, animated: true)
    
  }
  
}
