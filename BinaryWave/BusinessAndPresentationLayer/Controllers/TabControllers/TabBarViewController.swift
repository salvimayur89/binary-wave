//
//  TabBarViewController.swift
//  BinaryWave
//
//  Created by Neosoft on 07/02/22.
//

import UIKit

class TabBarViewController: UITabBarController,UITabBarControllerDelegate {
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.delegate = self
    let Home = createTab(vc: HomeViewController(),
                         title: "",
                         imageName: "TT-grey",
                         tag: 1,
                         selectedImageName: "TT-blue")
    
    let Home1 = createTab(vc: MusicViewController(),
                          title: "",
                          imageName: "Movie-grey",
                          tag: 1,
                          selectedImageName: "Movie-blue")
    
    let Home2 = createTab(vc: DownloadsViewController(),
                          title: "",
                          imageName: "Live-grey",
                          tag: 1,
                          selectedImageName: "Live-blue")
    
    let Home3 = createTab(vc: MusicViewController(),
                          title: "",
                          imageName: "Downloads-grey",
                          tag: 1,
                          selectedImageName: "Downloads-blue")
    let Home4 = createTab(vc: DownloadsViewController(),
                          title: "",
                          imageName: "Music-grey",
                          tag: 1,
                          selectedImageName: "Music-blue")
    
    
    self.viewControllers = [Home,Home1,Home2,Home4,Home3]
    self.tabBar.tintColor = .clear
    
    self.selectedIndex = 0
    
    if #available(iOS 13.0, *) {
      let tabBarAppearance: UITabBarAppearance = UITabBarAppearance()
      tabBarAppearance.configureWithDefaultBackground()
      tabBarAppearance.backgroundColor = UIColor.black
      UITabBar.appearance().standardAppearance = tabBarAppearance
      
      if #available(iOS 15.0, *) {
        UITabBar.appearance().scrollEdgeAppearance = tabBarAppearance
      }
    }
  }
  
  func createTab(vc:UIViewController , title: String? , imageName:String , tag:Int,selectedImageName:String)->UINavigationController {
    let navgationC = UINavigationController(rootViewController: vc)
    let image: UIImage? = UIImage(named:imageName)?.withRenderingMode(.alwaysOriginal)
    let selectedImage = UIImage(named:selectedImageName)?.withRenderingMode(.alwaysOriginal)
    
    let tabBarItem = UITabBarItem(title: nil, image:image, tag: tag)
    tabBarItem.selectedImage = selectedImage
    navgationC.tabBarItem = tabBarItem
    return navgationC
  }
}
