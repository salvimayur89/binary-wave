//
//  BaseViewController.swift
//  BinaryWave
//
//  Created by Neosoft on 07/02/22.
//

import UIKit

class BaseViewController: UIViewController {
  
  override func viewDidLoad() {
    
    super.viewDidLoad()
    setupView()
  }
  
  // Common function to customise Viewe
  func setupView() {
    let appearance = UINavigationBarAppearance()
    appearance.configureWithOpaqueBackground()
    appearance.titleTextAttributes = [.font: UIFont.boldSystemFont(ofSize: 18.0),
                                      .foregroundColor: UIColor.white]
    appearance.backgroundColor = .black
    
    appearance.backgroundColor = .black
    
    let searchButton = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(actnSearchPressed))
    searchButton.tintColor = UIColor.white
    self.navigationItem.rightBarButtonItem  = searchButton
    
    navigationController?.navigationBar.standardAppearance = appearance
    navigationController?.navigationBar.scrollEdgeAppearance = appearance
  
  }
  
  @objc func actnSearchPressed(){
    print("Search")
  }
  
}
