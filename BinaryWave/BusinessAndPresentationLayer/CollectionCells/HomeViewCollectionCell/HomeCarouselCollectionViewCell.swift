//
//  HomeCarouselCollectionViewCell.swift
//  BinaryWave
//
//  Created by Neosoft on 08/02/22.
//

import UIKit

class HomeCarouselCollectionViewCell: UICollectionViewCell {

    @IBOutlet var imgCarousel: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
