//
//  CircularRailCollectionViewCell.swift
//  BinaryWave
//
//  Created by Neosoft on 08/02/22.
//

import UIKit
import SDWebImage

class CircularRailCollectionViewCell: UICollectionViewCell {
  
  
  @IBOutlet weak var imgThumbnail: UIImageView!
  @IBOutlet weak var labelTitle: UILabel!
  
  //  var railData: TracksLst?
  
  var trackData : TracksLst? {
    didSet{
      guard let track = trackData else { return}
      
      if let title = track.trackEnName {
        labelTitle.text = title
      }
      
      if let imageUrl = track.circularThumbnail, !imageUrl.isEmpty {
        imgThumbnail.sd_setImage(with: URL(string: URLs.circularImageBaseURL + imageUrl), placeholderImage: UIImage(named: "Rounded")) { image, error, cacheType, url in
        }
      }
    }
  }
  
  
  
  
  override func awakeFromNib() {
    super.awakeFromNib()
    setupView()
    
  }
  
  func setupView() {
    imgThumbnail.layer.borderWidth = 1
    imgThumbnail.layer.masksToBounds = false
    imgThumbnail.layer.borderColor = UIColor.black.cgColor
    imgThumbnail.layer.cornerRadius = imgThumbnail.frame.height/2
    imgThumbnail.clipsToBounds = true
  }
  
}
