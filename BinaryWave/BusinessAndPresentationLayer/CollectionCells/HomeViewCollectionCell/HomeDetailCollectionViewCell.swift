//
//  HomeDetailCollectionViewCell.swift
//  BinaryWave
//
//  Created by Neosoft on 09/02/22.
//

import UIKit

class HomeDetailCollectionViewCell: UICollectionViewCell {
    @IBOutlet var bgView: UIView!
    @IBOutlet var moreImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialSetup()
        // Initialization code
    }
    func initialSetup(){
        bgView.layer.cornerRadius = 10
        bgView.clipsToBounds = true
        moreImageView.layer.cornerRadius = 10
        moreImageView.clipsToBounds = true
    }

}
