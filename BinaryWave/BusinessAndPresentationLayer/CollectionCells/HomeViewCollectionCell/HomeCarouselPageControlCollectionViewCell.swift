//
//  HomeCarouselPageControlCollectionViewCell.swift
//  BinaryWave
//
//  Created by Neosoft on 09/02/22.
//

import UIKit

class HomeCarouselPageControlCollectionViewCell: UICollectionViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = 5
        contentView.clipsToBounds = true
        contentView.backgroundColor = UIColor.gray
    }

}
