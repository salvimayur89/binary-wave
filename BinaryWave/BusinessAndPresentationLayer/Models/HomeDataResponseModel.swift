//
//  HomeDataResponseModel.swift
//  BinaryWave
//
//  Created by Neosoft on 08/02/22.
//

import Foundation
import Alamofire

struct HomeDataResponseModel: Codable {
    let railLst: [RailLst]?
    let carouselLst: [CarouselLst]?

    enum CodingKeys: String, CodingKey {
        case railLst = "RailLst"
        case carouselLst = "CarouselLst"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        railLst = try values.decodeIfPresent([RailLst].self, forKey: .railLst)
        carouselLst = try values.decodeIfPresent([CarouselLst].self, forKey: .carouselLst)
    }
}
struct CarouselLst: Codable {
    let contentTypeID: Int?
    let contentType: String?
    let contentID: Int?
    let contentOrder: Int?
    let carouselImage: String?

    enum CodingKeys: String, CodingKey {
        case contentTypeID = "ContentTypeID"
        case contentType = "ContentType"
        case contentID = "ContentID"
        case contentOrder = "ContentOrder"
        case carouselImage = "CarouselImage"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        contentTypeID = try values.decodeIfPresent(Int.self, forKey: .contentTypeID)
        contentType = try values.decodeIfPresent(String.self, forKey: .contentType)
        contentID = try values.decodeIfPresent(Int.self, forKey: .contentID)
        contentOrder = try values.decodeIfPresent(Int.self, forKey: .contentOrder)
        carouselImage = try values.decodeIfPresent(String.self, forKey: .carouselImage)
    }
}

// MARK: - RailLst
struct RailLst: Codable {
    let railID: Int?
    let railName: String?
    let railArName: String?
    let railOrder: Int?
    let railTypeID: Int?
    let contentTypeID: Int?
    let imageShape: String?
    let contentType: String?
    let tracksLst: [TracksLst]?
    let albumsLst: [AlbumsLst]?
    let videoLst: [VideoLst]?
    let radioLst: [RadioLst]?
    let singerLst: [SingerLst]?
    let playLst: [PlayLst]?

    enum CodingKeys: String, CodingKey {
        case railID = "RailID"
        case railName = "RailName"
        case railArName = "RailArName"
        case railOrder = "RailOrder"
        case railTypeID = "RailTypeID"
        case contentTypeID = "ContentTypeID"
        case imageShape = "ImageShape"
        case contentType = "ContentType"
        case tracksLst = "TracksLst"
        case albumsLst = "AlbumsLst"
        case videoLst = "VideoLst"
        case radioLst = "RadioLst"
        case singerLst = "SingerLst"
        case playLst = "PlayLst"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        railID = try values.decodeIfPresent(Int.self, forKey: .railID)
        railName = try values.decodeIfPresent(String.self, forKey: .railName)
        railArName = try values.decodeIfPresent(String.self, forKey: .railArName)
        railOrder = try values.decodeIfPresent(Int.self, forKey: .railOrder)
        railTypeID = try values.decodeIfPresent(Int.self, forKey: .railTypeID)
        contentTypeID = try values.decodeIfPresent(Int.self, forKey: .contentTypeID)
        imageShape = try values.decodeIfPresent(String.self, forKey: .imageShape)
        contentType = try values.decodeIfPresent(String.self, forKey: .contentType)
        tracksLst = try values.decodeIfPresent([TracksLst].self, forKey: .tracksLst)
        albumsLst = try values.decodeIfPresent([AlbumsLst].self, forKey: .albumsLst)
        videoLst = try values.decodeIfPresent([VideoLst].self, forKey: .videoLst)
        radioLst = try values.decodeIfPresent([RadioLst].self, forKey: .radioLst)
        singerLst = try values.decodeIfPresent([SingerLst].self, forKey: .singerLst)
        playLst = try values.decodeIfPresent([PlayLst].self, forKey: .playLst)
    }
}

// MARK: - AlbumsLst
struct AlbumsLst: Codable {
    let albumID, singerID: Int?
    let singerEnName, singerArName: String?
    let singerImg: String?
    let albumEnName, albumArName, albumImgPath: String?
    let albumImgShape: String?
    let userLastListenedTrack: Int?

    enum CodingKeys: String, CodingKey {
        case albumID = "AlbumId"
        case singerID = "SingerId"
        case singerEnName = "SingerEnName"
        case singerArName = "SingerArName"
        case singerImg = "SingerImg"
        case albumEnName = "AlbumEnName"
        case albumArName = "AlbumArName"
        case albumImgPath = "AlbumImgPath"
        case albumImgShape = "AlbumImgShape"
        case userLastListenedTrack = "UserLastListenedTrack"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        albumID = try values.decodeIfPresent(Int.self, forKey: .albumID)
        singerID = try values.decodeIfPresent(Int.self, forKey: .singerID)
        singerEnName = try values.decodeIfPresent(String.self, forKey: .singerEnName)
        singerArName = try values.decodeIfPresent(String.self, forKey: .singerArName)
        singerImg = try values.decodeIfPresent(String.self, forKey: .singerImg)
        albumEnName = try values.decodeIfPresent(String.self, forKey: .albumEnName)
        albumArName = try values.decodeIfPresent(String.self, forKey: .albumArName)
        albumImgPath = try values.decodeIfPresent(String.self, forKey: .albumImgPath)
        albumImgShape = try values.decodeIfPresent(String.self, forKey: .albumImgShape)
        userLastListenedTrack = try values.decodeIfPresent(Int.self, forKey: .userLastListenedTrack)
    }
}


// MARK: - PlayLst
struct PlayLst: Codable {
    let playlistID: Int?
    let playlistName, playlistArName: String?
    let playlistArDesc, playlistEnDesc: String?
    let playlistImgPath: String?
    let userLastListenedTrack: Int?

    enum CodingKeys: String, CodingKey {
        case playlistID = "PlaylistId"
        case playlistName = "PlaylistName"
        case playlistArName = "PlaylistArName"
        case playlistArDesc = "PlaylistArDesc"
        case playlistEnDesc = "PlaylistEnDesc"
        case playlistImgPath = "PlaylistImgPath"
        case userLastListenedTrack = "UserLastListenedTrack"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        playlistID = try values.decodeIfPresent(Int.self, forKey: .playlistID)
        playlistName = try values.decodeIfPresent(String.self, forKey: .playlistName)
        playlistArName = try values.decodeIfPresent(String.self, forKey: .playlistArName)
        playlistArDesc = try values.decodeIfPresent(String.self, forKey: .playlistArDesc)
        playlistEnDesc = try values.decodeIfPresent(String.self, forKey: .playlistEnDesc)
        playlistImgPath = try values.decodeIfPresent(String.self, forKey: .playlistImgPath)
        userLastListenedTrack = try values.decodeIfPresent(Int.self, forKey: .userLastListenedTrack)
    }
}

// MARK: - RadioLst
struct RadioLst: Codable {
    let radioID: Int?
    let radioName, radioArName: String?
    let radioURL: String?
    let radioImage: String?

    enum CodingKeys: String, CodingKey {
        case radioID = "RadioId"
        case radioName = "RadioName"
        case radioArName = "RadioArName"
        case radioURL = "RadioURL"
        case radioImage = "RadioImage"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        radioID = try values.decodeIfPresent(Int.self, forKey: .radioID)
        radioName = try values.decodeIfPresent(String.self, forKey: .radioName)
        radioArName = try values.decodeIfPresent(String.self, forKey: .radioArName)
        radioURL = try values.decodeIfPresent(String.self, forKey: .radioURL)
        radioImage = try values.decodeIfPresent(String.self, forKey: .radioImage)
    }
}

// MARK: - SingerLst
struct SingerLst: Codable {
    let singerID: Int?
    let nationalityArName: String?
    let nationalityEnName: String?
    let singerEnName, singerArName, singerImgPath: String?

    enum CodingKeys: String, CodingKey {
        case singerID = "SingerId"
        case nationalityArName = "NationalityArName"
        case nationalityEnName = "NationalityEnName"
        case singerEnName = "SingerEnName"
        case singerArName = "SingerArName"
        case singerImgPath = "SingerImgPath"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        singerID = try values.decodeIfPresent(Int.self, forKey: .singerID)
        nationalityArName = try values.decodeIfPresent(String.self, forKey: .nationalityArName)
        nationalityEnName = try values.decodeIfPresent(String.self, forKey: .nationalityEnName)
        singerEnName = try values.decodeIfPresent(String.self, forKey: .singerEnName)
        singerArName = try values.decodeIfPresent(String.self, forKey: .singerArName)
        singerImgPath = try values.decodeIfPresent(String.self, forKey: .singerImgPath)
    }
}


// MARK: - TracksLst
struct TracksLst: Codable {
    let trackIndex, trackID, videoID: Int?
    let trackEnName, trackArName, trackPath: String?
    let isFavourite, isRBT, hasLyrics: Bool?
    let lyricFile: String?
    let trackRating, trackLength: Int?
    let likesCount, listenCount, recommendedText, recommendedArText: String?
    let trackImage, carouselThumbnail, circularThumbnail, musicThumbnail2: String?
    let isPremium, isDownloaded: Bool?
    let listenedDuration: String?
    let contentType, userInterested, albumID, singerID: Int?
    let singerEnName, singerArName, singerImg: String?
    let albumEnName: String?
    let albumArName: String?
    let albumImgPath: String?
    let albumImgShape: String?
    let userLastListenedTrack: Int?

    enum CodingKeys: String, CodingKey {
        case trackIndex = "trackIndex"
        case trackID = "TrackId"
        case videoID = "VideoID"
        case trackEnName = "TrackEnName"
        case trackArName = "TrackArName"
        case trackPath = "TrackPath"
        case isFavourite = "IsFavourite"
        case isRBT = "IsRBT"
        case hasLyrics = "HasLyrics"
        case lyricFile = "LyricFile"
        case trackRating = "TrackRating"
        case trackLength = "TrackLength"
        case likesCount = "LikesCount"
        case listenCount = "ListenCount"
        case recommendedText = "RecommendedText"
        case recommendedArText = "RecommendedArText"
        case trackImage = "TrackImage"
        case carouselThumbnail = "Carousel_thumbnail"
        case circularThumbnail = "Circular_thumbnail"
        case musicThumbnail2 = "Music_thumbnail_2"
        case isPremium = "IsPremium"
        case isDownloaded = "IsDownloaded"
        case listenedDuration = "ListenedDuration"
        case contentType = "ContentType"
        case userInterested = "UserInterested"
        case albumID = "AlbumId"
        case singerID = "SingerId"
        case singerEnName = "SingerEnName"
        case singerArName = "SingerArName"
        case singerImg = "SingerImg"
        case albumEnName = "AlbumEnName"
        case albumArName = "AlbumArName"
        case albumImgPath = "AlbumImgPath"
        case albumImgShape = "AlbumImgShape"
        case userLastListenedTrack = "UserLastListenedTrack"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        trackIndex = try values.decodeIfPresent(Int.self, forKey: .trackIndex)
        trackID = try values.decodeIfPresent(Int.self, forKey: .trackID)
        videoID = try values.decodeIfPresent(Int.self, forKey: .videoID)
        trackEnName = try values.decodeIfPresent(String.self, forKey: .trackEnName)
        trackArName = try values.decodeIfPresent(String.self, forKey: .trackArName)
        trackPath = try values.decodeIfPresent(String.self, forKey: .trackPath)
        isFavourite = try values.decodeIfPresent(Bool.self, forKey: .isFavourite)
        isRBT = try values.decodeIfPresent(Bool.self, forKey: .isRBT)
        hasLyrics = try values.decodeIfPresent(Bool.self, forKey: .hasLyrics)
        lyricFile = try values.decodeIfPresent(String.self, forKey: .lyricFile)
        trackRating = try values.decodeIfPresent(Int.self, forKey: .trackRating)
        trackLength = try values.decodeIfPresent(Int.self, forKey: .trackLength)
        likesCount = try values.decodeIfPresent(String.self, forKey: .likesCount)
        listenCount = try values.decodeIfPresent(String.self, forKey: .listenCount)
        recommendedText = try values.decodeIfPresent(String.self, forKey: .recommendedText)
        recommendedArText = try values.decodeIfPresent(String.self, forKey: .recommendedArText)
        trackImage = try values.decodeIfPresent(String.self, forKey: .trackImage)
        carouselThumbnail = try values.decodeIfPresent(String.self, forKey: .carouselThumbnail)
        circularThumbnail = try values.decodeIfPresent(String.self, forKey: .circularThumbnail)
        musicThumbnail2 = try values.decodeIfPresent(String.self, forKey: .musicThumbnail2)
        isPremium = try values.decodeIfPresent(Bool.self, forKey: .isPremium)
        isDownloaded = try values.decodeIfPresent(Bool.self, forKey: .isDownloaded)
        listenedDuration = try values.decodeIfPresent(String.self, forKey: .listenedDuration)
        contentType = try values.decodeIfPresent(Int.self, forKey: .contentType)
        userInterested = try values.decodeIfPresent(Int.self, forKey: .userInterested)
        albumID = try values.decodeIfPresent(Int.self, forKey: .albumID)
        singerID = try values.decodeIfPresent(Int.self, forKey: .singerID)
        singerEnName = try values.decodeIfPresent(String.self, forKey: .singerEnName)
        singerArName = try values.decodeIfPresent(String.self, forKey: .singerArName)
        singerImg = try values.decodeIfPresent(String.self, forKey: .singerImg)
        albumEnName = try values.decodeIfPresent(String.self, forKey: .albumEnName)
        albumArName = try values.decodeIfPresent(String.self, forKey: .albumArName)
        albumImgPath = try values.decodeIfPresent(String.self, forKey: .albumImgPath)
        albumImgShape = try values.decodeIfPresent(String.self, forKey: .albumImgShape)
        userLastListenedTrack = try values.decodeIfPresent(Int.self, forKey: .userLastListenedTrack)
    }
}

// MARK: - VideoLst
struct VideoLst: Codable {
    let videoID: Int?
    let videoEnName, videoArName, videoPath, videoPoster: String?
    let singerID: Int?
    let singerEnName, singerArName, likesCount, viewCount: String?
    let isFavourite, isPremium, isDownloaded: Bool?
    let viewedDuration: String?
    let contentType, userInterested: Int?

    enum CodingKeys: String, CodingKey {
        case videoID = "VideoID"
        case videoEnName = "VideoEnName"
        case videoArName = "VideoArName"
        case videoPath = "VideoPath"
        case videoPoster = "VideoPoster"
        case singerID = "SingerId"
        case singerEnName = "SingerEnName"
        case singerArName = "SingerArName"
        case likesCount = "LikesCount"
        case viewCount = "ViewCount"
        case isFavourite = "IsFavourite"
        case isPremium = "IsPremium"
        case isDownloaded = "IsDownloaded"
        case viewedDuration = "ViewedDuration"
        case contentType = "ContentType"
        case userInterested = "UserInterested"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        videoID = try values.decodeIfPresent(Int.self, forKey: .videoID)
        videoEnName = try values.decodeIfPresent(String.self, forKey: .videoEnName)
        videoArName = try values.decodeIfPresent(String.self, forKey: .videoArName)
        videoPath = try values.decodeIfPresent(String.self, forKey: .videoPath)
        videoPoster = try values.decodeIfPresent(String.self, forKey: .videoPoster)
        singerID = try values.decodeIfPresent(Int.self, forKey: .singerID)
        singerEnName = try values.decodeIfPresent(String.self, forKey: .singerEnName)
        singerArName = try values.decodeIfPresent(String.self, forKey: .singerArName)
        likesCount = try values.decodeIfPresent(String.self, forKey: .likesCount)
        viewCount = try values.decodeIfPresent(String.self, forKey: .viewCount)
        isFavourite = try values.decodeIfPresent(Bool.self, forKey: .isFavourite)
        isPremium = try values.decodeIfPresent(Bool.self, forKey: .isPremium)
        isDownloaded = try values.decodeIfPresent(Bool.self, forKey: .isDownloaded)
        viewedDuration = try values.decodeIfPresent(String.self, forKey: .viewedDuration)
        contentType = try values.decodeIfPresent(Int.self, forKey: .contentType)
        userInterested = try values.decodeIfPresent(Int.self, forKey: .userInterested)
    }
}
