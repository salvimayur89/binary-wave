//
//  HomeCarouselTableViewCell.swift
//  BinaryWave
//
//  Created by Neosoft on 08/02/22.
//

import UIKit
import SDWebImage

class HomeCarouselTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionViewCarousel: UICollectionView!
    @IBOutlet var pageControllCollectionView: UICollectionView!
  
  
    var kSquareRailCell = "HomeCarouselCollectionViewCell"
    let kPageControlCell = "HomeCarouselPageControlCollectionViewCell"
    var carouselLst: [CarouselLst]?
    var timer = Timer()
    var counter = 0
    override func awakeFromNib() {
        super.awakeFromNib()
        DispatchQueue.main.async {
            self.timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.changeImage), userInfo: nil, repeats: true)
        }
        collectionViewCarousel.delegate = self
        collectionViewCarousel.dataSource = self
        collectionViewCarousel.register(UINib(nibName: kSquareRailCell, bundle: nil), forCellWithReuseIdentifier: kSquareRailCell)
        pageControllCollectionView.delegate = self
        pageControllCollectionView.dataSource = self
        pageControllCollectionView.register(UINib(nibName: kPageControlCell, bundle: nil), forCellWithReuseIdentifier: kPageControlCell)
    }
    
    func setCarouselData(carouselLst: [CarouselLst]?){
        self.carouselLst = carouselLst
        collectionViewCarousel.reloadData()
    }
    @objc func changeImage(){
        if let carouselCount = carouselLst?.count{
            if counter < carouselCount{
                setCollectionIndex()
                counter += 1
            }else{
                counter = 0
                setCollectionIndex()
            }
        }
    }
    func setCollectionIndex(){
        let index = IndexPath.init(item: counter, section: 0)
        collectionViewCarousel.scrollToItem(at: index, at: .centeredHorizontally, animated: true)
        changeBGColorOfPageCollection(index: counter,color: UIColor(red: 0, green: 181, blue: 255, alpha: 0.8))
        if let list = self.carouselLst?.count, list != 0{
            let changeIndex = counter > 0 ? counter - 1 : list - 1
            changeBGColorOfPageCollection(index: changeIndex,color: UIColor.gray)
        }
    }
    
    func changeBGColorOfPageCollection(index: Int, color: UIColor){
        let indexPath = IndexPath.init(item: index, section: 0)
        let cell = pageControllCollectionView?.cellForItem(at: indexPath)
        cell?.contentView.backgroundColor = color
    }
}

extension HomeCarouselTableViewCell: UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
           return carouselLst?.count ?? 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        if(collectionView == self.collectionViewCarousel){
            let cell = collectionViewCarousel.dequeueReusableCell(withReuseIdentifier: kSquareRailCell, for: indexPath) as! HomeCarouselCollectionViewCell
            if let image = carouselLst?[indexPath.row].carouselImage{
                let url = URLs.carouselImageBaseURL + image
                cell.imgCarousel.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "placeholderImg")) { image, error, cacheType, url in
                }
            }
            return cell
        }else{
            let cell = pageControllCollectionView.dequeueReusableCell(withReuseIdentifier: kPageControlCell, for: indexPath) as! HomeCarouselPageControlCollectionViewCell
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        if(collectionView == self.collectionViewCarousel){
           return CGSize(width: collectionViewCarousel.frame.width, height: 183)
        }else{
            return CGSize(width: 30, height: 5)
        }
    }
  
  
  
  func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
      for cell in collectionViewCarousel.visibleCells {
        if let row = collectionViewCarousel.indexPath(for: cell)?.item {
             print(row)
        }
      }
  }
}


