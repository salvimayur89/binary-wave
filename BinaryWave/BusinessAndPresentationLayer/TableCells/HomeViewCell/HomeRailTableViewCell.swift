//
//  HomeRailTableViewCell.swift
//  BinaryWave
//
//  Created by Neosoft on 07/02/22.
//

import UIKit

class HomeRailTableViewCell: UITableViewCell {
  
  @IBOutlet weak var labelTitle: UILabel!
  @IBOutlet weak var btnMoreOptions: UIButton!
  @IBOutlet weak var collectionViewHomeRails: UICollectionView!
  
  //CollectionView Cell Identifiers
  var kCircularRailCell = "CircularRailCollectionViewCell"
  var kSquareRailCell = "SquareRailCollectionViewCell"
  
  override func awakeFromNib() {
    super.awakeFromNib()
    collectionViewHomeRails.register(UINib(nibName: kCircularRailCell, bundle: nil), forCellWithReuseIdentifier: kCircularRailCell)
    collectionViewHomeRails.register(UINib(nibName: kSquareRailCell, bundle: nil), forCellWithReuseIdentifier: kSquareRailCell)
  }
  
  
}
