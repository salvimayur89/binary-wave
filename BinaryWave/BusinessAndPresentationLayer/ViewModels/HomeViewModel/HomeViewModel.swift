//
//  HomeViewModel.swift
//  BinaryWave
//
//  Created by Neosoft on 08/02/22.
//

import Foundation
import UIKit

class HomeViewModel{
  var responseData:HomeDataResponseModel?
  
  // MARK: - API Calls
  
  func getHomePageData(genre: String, completion: @escaping(String) -> Void) {
    let iNetwork = NetworkManager.sharedInstance
    //    let url = URLs.baseURL + URLs.getApi(webService: .homeListWithGenre) + "\(userId)" + "&genreId=\(genre)&userToken=\(userToken)"
    
    let url = "http://184.105.143.224/TenTimeWS/TenTimeContent.asmx/GetHomelistsWithRail?userId=25524&genreId=1&userToken=tt2u4iKsfz1KIf8Zf1kr5DaTkmS3QpCrz/Y9MLgs7oTfhwDeFimFiLl6+KJ3dEJRucTNdcGiIiRZgkryLarJjI1stnuQDujAv2LSxung6Pl5DVI/cJZy5uHh2hpU6tpAi3jVcyDiGq4WoSs8EjLiClu0DCtPQwf3L3qR+m977zQ7skOqiQT30mMeCa9Lnh/HZC7vOu7F0C8PI83AtQ1EcNyE7Zp8ptj7cp41yoxjiun/NT4NthsjsvZHZjITRXKXOCZ4xkzYGfqHtG63xvekMx1epbW87/tI6BIeTeo1LFunnGnPMaPOmwUCqCkFrrfXWHZ7eF2NpuSJ4hIkQekWo6VI3VbfK700UP9vv7E84rGHsBRz8aU+wyO6Xmpekal378M9ZwQiliTJkL0yNY5s+X2XlPbjDQY5RwkPVJnKsOIG+zNAde6yu6a5rxxmDLQK2xy5Tnh9gMDCwTSmRDn8lczKNMsL/13Mym7TO1UCILY2PxGoja8GmAbEvPxHo/94co5BA8sy46L/XbLfvLb/QfYPZHc5l0VTjR+IIYZuInJR8j8vN9+WRYWlCiHVpZAQpAOuHTLcVyUan87LQx7uBwTqkSn0KCM3oMj6F3cXpjXxchXKN32K+Oh6ANxPklz5"
    
    iNetwork.performRequest(baseUrl: url, isHud: true, objectType: HomeDataResponseModel.self, method: .get, param: nil, success: { data in
      self.responseData = data
      completion("")
      
    }) { (error) in
      completion(error.errorMessage)
    }
  }
}


// MARK: - Fetch Data
extension HomeViewModel {
  
  func getCarouselCount() -> Int {
    if let data = getCarouselData() {
      return data.count
    }
    
    return 0
  }
  
  func getRailsCount() -> Int {
    if let data = getRailsData() {
      return data.count
    }
    return 0
  }
  
  func getCarouselData() -> [CarouselLst]? {
    if let data = responseData, let carouselData = data.carouselLst {
      return carouselData
    }
    return nil
  }
  
  func getRailsData() -> [RailLst]? {
    if let data = responseData, let railsData = data.railLst {
      return railsData
    }
    return nil
  }
  
  func getRail(index: Int)-> RailLst? {
    if let data = getRailsData(), data.count > index {
      return data[index]
    }
    return nil
  }
  
  func getRailTitle(index:Int) -> String {
    if let rail = getRail(index: index), let name = rail.railName {
      return name
    }
    return ""
  }
  
  func getRailCategory(index:Int) -> String {
    if let rail = getRail(index: index), let shape = rail.imageShape {
      return shape
    }
    return ""
  }
  
  func getRailContentType(index:Int) -> String? {
    if let rail = getRail(index: index), let shape = rail.contentType {
      return shape
    }
    return nil
  }
  
  func getTrackData(section:Int) -> [TracksLst]? {
    if let railData = getRail(index: section), let trackData = railData.tracksLst {
      return trackData
    }
    return nil
  }
  
  func getRailTrack(row:Int,section:Int) -> TracksLst? {
    if let trackData = getTrackData(section: section), trackData.count > row {
      return trackData[row]
    }
    return nil
    
  }
  
  func getRailImageURl(row:Int,section:Int) -> String?{
    
    if let railContentType = getRailContentType(index: section) {
      
      switch railContentType {
      case "Audio":
        if let data = getTrackData(section: section), data.count > row {
          if getRailCategory(index: section) == "circle" {
            if let thumbnail = data[row].circularThumbnail {
              return URLs.circularImageBaseURL + thumbnail
            }
          } else {
            if let thumbnail = data[row].trackImage {
              return URLs.imageBaseURL + thumbnail
            }
          }
        }
      case "Albums":
        if let railData = getRail(index: section), let albmData = railData.albumsLst, albmData.count > row, let thumbnail = albmData[row].albumImgPath{
          return URLs.imageBaseURL + thumbnail
        }
      case "Video":
        if let railData = getRail(index: section), let videoData = railData.videoLst, videoData.count > row, let thumbnail = videoData[row].videoPoster{
          return URLs.imageBaseURL + thumbnail
        }
      case "Radio":
        if let railData = getRail(index: section), let radioData = railData.radioLst, radioData.count > row, let thumbnail = radioData[row].radioImage{
          return URLs.imageBaseURL + thumbnail
        }
      case "Singer":
        if let railData = getRail(index: section), let singerData = railData.singerLst, singerData.count > row, let thumbnail = singerData[row].singerImgPath{
          return URLs.singerImageBaseURL + thumbnail
        }
      case "PlayList":
        if let railData = getRail(index: section), let playListData = railData.playLst, playListData.count > row, let thumbnail = playListData[row].playlistImgPath{
          return URLs.playListImageBaseURL + thumbnail
        }
        
      default:
        return nil
      }
    }
    return nil
  }
  
  func getRailDataCount(section:Int) -> Int{
    
    if let railContentType = getRailContentType(index: section) {
      
      switch railContentType {
      case "Audio":
        if let data = getTrackData(section: section){
          return data.count
        }
      case "Albums":
        if let railData = getRail(index: section), let albmData = railData.albumsLst{
          return albmData.count
        }
      case "Video":
        if let railData = getRail(index: section), let videoData = railData.videoLst{
          return videoData.count
        }
      case "Radio":
        if let railData = getRail(index: section), let radioData = railData.radioLst{
          return radioData.count
        }
      case "Singer":
        if let railData = getRail(index: section), let singerData = railData.singerLst {
          return singerData.count
        }
      case "PlayList":
        if let railData = getRail(index: section), let playListData = railData.playLst {
          return playListData.count
        }
        
      default:
        return 0
      }
    }
    return 0
  }
  
}
