//
//  NetworkManager.swift
//  BinaryWave
//
//  Created by Neosoft on 07/02/22.
//

import Foundation
import Alamofire

final class NetworkManager {
    static var sharedInstance = NetworkManager()
    private init(){}
    
    //Added
    //completion: @escaping responseHandler
    func performRequest<T:Codable>(baseUrl:String, isHud: Bool, objectType : T.Type, method:HTTPMethod, param: Parameters?, success : @escaping (_ responseData : T) -> Void, failure:@escaping (ErrorObject) -> Void) {
        print("url: \(baseUrl)")
        if ((NetworkReachabilityManager()?.isReachable) != nil) {
            if isHud{
                DispatchQueue.main.async {
                    CustomLoaderView.showLoader()
                }
            }
            AF.sessionConfiguration.timeoutIntervalForResource = 60
            AF.sessionConfiguration.timeoutIntervalForRequest = 60
            
            AF.request(baseUrl, method: method, parameters: param, encoding: JSONEncoding.default).responseJSON { response in
                print("Parameters : \(param as Any)")                
                if isHud{
                    DispatchQueue.main.async {
                        CustomLoaderView.dismiss()
                    }
                }
                switch (response.result) {
                case .success:
                    if let data = response.data {
                        if let baseResponse = try? JSONDecoder().decode(T.self, from: data){
                            print("success Response : \(baseResponse)")
                            print("response : \(response.result)")
                             DispatchQueue.main.async {
                                success(baseResponse)
                             }
                        } else {
                            print("HIIii")
                        }  //this is how can bind the data to models
                    }
                    break
                case .failure(let error):
                    print("Failure Response : \(response)")
                    print(error.localizedDescription)
                    let errorObj = ErrorObject(errCode: NSURLErrorTimedOut, errMessage: error.localizedDescription)
                    failure(errorObj)
                }
            }
        } else {
            let errorObj = ErrorObject(errCode: NSURLErrorNotConnectedToInternet, errMessage: ConstantStrings.noInternetConnection)
            failure(errorObj)
        }
    }
    
    func performMultiPartRequest<T:Codable>(baseUrl:String, isHud: Bool, objectType : T.Type, fileData : Data?, fileName: String?, name: String?, mimeType: String?, method:HTTPMethod, params: Parameters?, headers:Headers, success : @escaping (_ responseData : T) -> Void, failure:@escaping (ErrorObject) -> Void) {
        if ((NetworkReachabilityManager()?.isReachable) != nil) {
            if isHud{
                DispatchQueue.main.async {
                    CustomLoaderView.showLoader()
                }
            }
            AF.sessionConfiguration.timeoutIntervalForResource = 60
            AF.sessionConfiguration.timeoutIntervalForRequest = 60
            
            AF.upload(multipartFormData: { multipartFormData in
                if fileData != nil {
                    multipartFormData.append(fileData!, withName: name!, fileName: fileName, mimeType: mimeType)
                }
                for (key, value) in params ?? [:] {
                    multipartFormData.append(((value as? String)?.data(using: .utf8))!, withName: key)
                }
            }, to: URL.init(string: baseUrl)!, headers: self.setHeaders(headers: headers)).uploadProgress(closure: { (Progress) in
                //print("Upload Progress: \(Progress.fractionCompleted)")
                DispatchQueue.main.async {
                    debugPrint(Progress)
                }
            }).responseJSON { (response) in
                print("Parameters : \(params as Any)")
                
                if isHud{
                    DispatchQueue.main.async {
                        CustomLoaderView.dismiss()
                    }
                }
                switch (response.result) {
                case .success:
                    if let data = response.data {
                        if let baseResponse = try? JSONDecoder().decode(T.self, from: data){
                            print("success Response : \(baseResponse)")
                            DispatchQueue.main.async {
                                  success(baseResponse)
                            }
                        }   //this is how can bind the data to models
                    }
                    break
                case .failure(let error):
                    print("Failure Response : \(response)")
                    print(error.localizedDescription)
                    let errorObj = ErrorObject(errCode: NSURLErrorTimedOut, errMessage: error.localizedDescription)
                    failure(errorObj)
                }
            }
        } else {
            let errorObj = ErrorObject(errCode: NSURLErrorNotConnectedToInternet, errMessage: ConstantStrings.noInternetConnection)
            failure(errorObj)
        }
    }
}

/*
 * Method name: NetworkService
 * Return: singleton
 */
extension NetworkManager {
    
    //MARK:-  Header
     func setHeaders(headers:Headers) -> HTTPHeaders {
        let strToken = ""
        let headers: HTTPHeaders = [
            "Content-Type": headers.ContentType,
            "Accept": headers.Accept,
            "Authorization": strToken
        ]
        return headers
    }
}
let strToken = ""

public struct HEADERS {
    static var urlEncoded  = Headers(ContentType: "application/x-www-form-urlencoded; charset=UTF-8", Accept: "application/json; charset=UTF-8", Authorization: strToken)
    static var appJson  = Headers(ContentType: "application/json; charset=UTF-8", Accept: "application/json; charset=UTF-8", Authorization: strToken)
    static var multipart  = Headers(ContentType: "multipart/form-data", Accept: "application/json; charset=UTF-8", Authorization: strToken)
}

struct Headers {
    let ContentType : String
    let Accept : String
    var Authorization: String
}

class ErrorObject: NSObject {
    var errorMessage: String = ""
    var errorCode: Int = 0
    
    init(errCode:Int,errMessage:String) {
        super.init()
        self.errorCode = errCode
        self.errorMessage = errMessage
    }
}

