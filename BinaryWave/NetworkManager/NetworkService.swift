//
//  NetworkService.swift
//  BinaryWave
//
//  Created by Neosoft on 07/02/22.
//

import Foundation
import UIKit
import Alamofire

let URLs = NetworkServices.sharedInstance
class NetworkServices: NSObject {
  /*
   * Method name: baseURL
   * Description: use to get network Environment
   * Return: environment
   */
  
  var networkEnvironment: NetworkEnvironment {
    return .Development
  }
  
  /*
   * Method name: baseURL
   * Description: use to get base of the service
   * Return: base url
   */
  
  var baseURL: String {
    switch networkEnvironment {
    case .Development:
      return "http://184.105.143.224/TenTimeWS/TenTimeContent.asmx/"
    case .Production:
      return ""
    }
  }
  var imageBaseURL: String {
    return "https://geo-img.tent-cdn.com/Images/Musics/Thumbnails/"
  }
  
  
  var circularImageBaseURL: String {
    return "https://geo-img.tent-cdn.com/Images/Musics-Circular/Thumbnails/"
  }
  
  var singerImageBaseURL: String {
    return "https://geo-img.tent-cdn.com/Images/Singers/Thumbnails/"
  }
  
  var playListImageBaseURL: String {
    return "https://geo-img.tent-cdn.com/Images/Playlist/Thumbnails/"
  }
  
  var carouselImageBaseURL: String {
    return "https://geo-img.tent-cdn.com/Images/Musics-Carousel/Thumbnails/"
  }
  
  
  func getApi(webService : WebService) -> String {
    switch webService {
    case .homeListWithGenre:
      return "GetHomelistsWithGenre?userId="
    default:
      return ""
    }
  }
}

/*
 * Method name: NetworkService
 * Description: This func use for shared Instance
 * Return: singleton
 */
extension NetworkServices {
  private static var privateShared : NetworkServices?
  class var sharedInstance :NetworkServices {
    struct Singleton {
      static let instance = NetworkServices()
    }
    return Singleton.instance
  }
}

