//
//  AppDelegate.swift
//  BinaryWave
//
//  Created by Neosoft on 07/02/22.
//

import UIKit

@UIApplicationMain


class AppDelegate: UIResponder, UIApplicationDelegate {
  
  var window: UIWindow?
  var navigationController = UINavigationController()
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    
    setupRootView()
    
    return true
  }
  
  
}

extension AppDelegate {
  
  func setupRootView() {
    NavigationHelper.launchApplication(navigate: .initialAppLaunch)
  }
  
}
