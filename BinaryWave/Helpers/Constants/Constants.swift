//
//  Constant.swift
//  BinaryWave
//
//  Created by Neosoft on 07/02/22.
//

import Foundation
import UIKit

 enum NetworkEnvironment: String {
     case Development
     case Production
 }
enum ConstantStrings {
    static let noInternetConnection = "Please check your internet connection and try again."
}
