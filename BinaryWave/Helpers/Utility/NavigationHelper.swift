//
//  NavigationHelper.swift
//  BinaryWave
//
//  Created by Neosoft on 07/02/22.
//

import Foundation
import UIKit

enum NavigationScreen {
  case initialAppLaunch
}

/**
 -- Thsi class is for supporting the navigation
 */
class NavigationHelper {
  
  static let appDelegate = UIApplication.shared.delegate as! AppDelegate
  
  private static func applicationStartWithScreen (screen: NavigationScreen) {
    
    appDelegate.navigationController.isNavigationBarHidden = true
    
    switch screen {
      
    case .initialAppLaunch:
      appDelegate.navigationController.viewControllers = [TabBarViewController()]
      
    }
    
    appDelegate.window?.rootViewController = appDelegate.navigationController
  }
  
  static func launchApplication(navigate: NavigationScreen) {
    self.applicationStartWithScreen(screen: navigate)
  }
  
  static func pushViewController(viewController: UIViewController) {
    appDelegate.navigationController.pushViewController(viewController, animated: true)
  }
  
  public static func setRootViewController(rootViewController: UIViewController) {
    appDelegate.navigationController.pushViewController(rootViewController, animated: true)
    appDelegate.navigationController.viewControllers = [rootViewController]
    appDelegate.window?.rootViewController = appDelegate.navigationController
  }
  
}

